/* 
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Other/SQLTemplate.sql to edit this template
 */
/**
 * Author:  student
 * Created: 28-Jun-2022
 */

DROP DATABASE IF EXISTS PARKING_SYSTEM_MANAGEMENT;
CREATE DATABASE PARKING_SYSTEM_MANAGEMENT;
USE PARKING_SYSTEM_MANAGEMENT;

CREATE TABLE IF NOT EXISTS VEHICLE_DETAILS(ID INT PRIMARY KEY AUTO_INCREMENT,OWNER_NAME VARCHAR(20) NOT NULL,VEHICLE_TYPE VARCHAR(10) NOT NULL);
DESC VEHICLE_DETAILS;

CREATE TABLE IF NOT EXISTS PARKING_DETAILS(ID INT PRIMARY KEY AUTO_INCREMENT,STATUS VARCHAR(10) NOT NULL,PARKING_FEE DECIMAL(8,2) NOT NULL,ENTRY_TIME TIME NOT NULL,EXIT_TIME TIME  NOT NULL,VEHICLE_ID INT NOT NULL,FOREIGN KEY(VEHICLE_ID) REFERENCES VEHICLE_DETAILS(ID));
DESC PARKING_DETAILS;