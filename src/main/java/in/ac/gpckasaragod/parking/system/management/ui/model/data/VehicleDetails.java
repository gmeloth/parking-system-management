/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.parking.system.management.ui.model.data;

import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class VehicleDetails {
    private Integer id;
    private String ownerName;
    private String vehicleType;

    public VehicleDetails(Integer id, String ownerName, String vehicleType) {
        this.id = id;
        this.ownerName = ownerName;
        this.vehicleType = vehicleType;
    }

    public Integer getId() {
        return id;
    }

    public String getOwnerName() {
        return ownerName;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setOwnerName(String ownerName) {
        this.ownerName = ownerName;
    }

    public void setVehicleType(String vehicleType) {
        this.vehicleType = vehicleType;
    }
    
    
}
