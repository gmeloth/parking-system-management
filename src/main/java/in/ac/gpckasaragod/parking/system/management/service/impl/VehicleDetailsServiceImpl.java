/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.parking.system.management.service.impl;

import in.ac.gpckasaragod.parking.system.management.ui.model.data.VehicleDetails;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import in.ac.gpckasaragod.parking.system.management.service.VehicleDetailsService;
import java.sql.PreparedStatement;
import java.util.List;

/**
 *
 * @author student
 */
public class VehicleDetailsServiceImpl extends ConnectionServiceImpl implements VehicleDetailsService{
    @Override
    public String saveVehicleDetails(String ownerName,String vehicleType) {
    try{
        Connection connection = getConnection();
        Statement statement = connection.createStatement();
        String query = "INSERT INTO VEHICLE_DETAILS(OWNER_NAME,VEHICLE_TYPE) VALUES " + "('"+ownerName+ "','"+vehicleType+"')";
        System.err.println("Query:"+query);
        int status = statement.executeUpdate(query);
        if(status != 1){
            return "Saved failed";
        }else{
            return "Saved successfully";
        }
        }catch (SQLException ex) {
            Logger.getLogger(VehicleDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
            return "save failed";
            
           }
        }
    @Override
          public VehicleDetails readVehicleDetails(Integer Id){
          VehicleDetails vehicledetails = null;     
      try {
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM VEHICLE_DETAILS WHERE ID="+Id;
          ResultSet resultSet = statement.executeQuery(query);
          while(resultSet.next()){
              int id =resultSet.getInt("ID");
              String owner_Name = resultSet.getString("OWNER_NAME");
              String vehicle_Type = resultSet.getString("VEHICLE_TYPE");
              vehicledetails = new VehicleDetails(id,owner_Name,vehicle_Type);
          }
      }catch (SQLException ex) {
          Logger.getLogger(VehicleDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      }      
       return vehicledetails;
          
          }
    @Override
          public List<VehicleDetails> getALLVehicleDetails(){
            List<VehicleDetails> vehicles = new ArrayList<>();
             try {
          Connection connection = getConnection();
          Statement statement = connection.createStatement();
          String query = "SELECT * FROM VEHICLE_DETAILS";
          ResultSet resultSet = statement.executeQuery(query);
            
          while(resultSet.next()){
              int id =resultSet.getInt("ID");
              String ownerName = resultSet.getString("OWNER_NAME");
              String vehicleType = resultSet.getString("VEHICLE_TYPE");
 VehicleDetails vehicleDetails = new VehicleDetails(id,ownerName,vehicleType);
 vehicles.add(vehicleDetails);
          }
             }    catch (SQLException ex) {
          Logger.getLogger(VehicleDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
      }      
       return vehicles;  
          }
    @Override
         public String updateVehicleDetails(Integer id,String ownerName,String vehicleType){
             try {
          Connection connection = getConnection();
          Statement statement = connection.createStatement(); 
         String query = "UPDATE VEHICLE_DETAILS SET OWNER_NAME='"+ownerName+"',VEHICLE_TYPE='"+vehicleType+"' WHERE ID="+id;
         System.out.print(query);
         int update = statement.executeUpdate(query);
         if(update !=1)
             return "Update failed";
         else
              return "Update successfully";
             }catch (SQLException ex) {
               Logger.getLogger(VehicleDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);  
             }
                 return "Update failed";
             }       

    @Override
    public String deleteVehicleDetails(Integer id) {
     try {
            Connection connection = getConnection();
            String query = "DELETE FROM VEHICLE_DETAILS WHERE ID =?";
            PreparedStatement statement = connection.prepareStatement(query);
            statement.setInt(1,id);
            int delete=statement.executeUpdate();
            if(delete !=1)
            return "Delete failed";
            else
            return "Delete successfully";
           } catch (SQLException ex) {
            Logger.getLogger(VehicleDetailsServiceImpl.class.getName()).log(Level.SEVERE,null,ex);
           }
            return "Delete failed";  
    }
         }  
    
        
    

             
  
