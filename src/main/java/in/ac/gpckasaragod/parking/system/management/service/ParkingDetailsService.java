/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.parking.system.management.service;

import in.ac.gpckasaragod.parking.system.management.ui.model.data.ParkingDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface ParkingDetailsService {
     public String saveParkingDetails(String status,String parkingFee,String entryTime,String exitTime,String vehicleId);
     public ParkingDetails readParkingDetails(Integer id);
     public List<ParkingDetails> getALLParkingDetails();
     public String updateParkingDetails(Integer id,String status,String parkingFee,String entryTime,String exitTime,String vehicleId);
     public String deleteParkingDetails(Integer id);
    
}
