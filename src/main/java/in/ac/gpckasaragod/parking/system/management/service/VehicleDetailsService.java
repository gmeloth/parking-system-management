/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package in.ac.gpckasaragod.parking.system.management.service;

import in.ac.gpckasaragod.parking.system.management.ui.model.data.VehicleDetails;
import java.util.List;

/**
 *
 * @author student
 */
public interface VehicleDetailsService {
     public String saveVehicleDetails(String ownerName,String vehicleType);
     public VehicleDetails readVehicleDetails(Integer id);
     public List<VehicleDetails> getALLVehicleDetails();
     public String updateVehicleDetails(Integer id,String ownername,String vehicletype);
     public String deleteVehicleDetails(Integer id);
}
    