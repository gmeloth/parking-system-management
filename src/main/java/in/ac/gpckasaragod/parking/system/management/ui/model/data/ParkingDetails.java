/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package in.ac.gpckasaragod.parking.system.management.ui.model.data;

import java.util.Date;
import java.util.logging.Logger;

/**
 *
 * @author student
 */
public class ParkingDetails {
  private Integer id;
    private String status;
    private Double parkingFee;
    private Date entryTime;
    private Date exitTime;
    private Integer vehicleId;
   